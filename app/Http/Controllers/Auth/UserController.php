<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::select('id','name')->withCount('favorites')->orderBy('favorites_count', 'desc')->get();

        return response()->json($users);
    }

    /**
     * Get authenticated user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function current(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Get all favorite photos by user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myFavorites()
    {
        $myFavorites = Auth::user()->favorites()->paginate(16);
        return response()->json($myFavorites);
    }
}
