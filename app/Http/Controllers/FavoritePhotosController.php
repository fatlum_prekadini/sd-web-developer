<?php

namespace App\Http\Controllers;

use App\FavoritePhotos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoritePhotosController extends Controller
{
    /**
     * List of favorite photos
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $photos = FavoritePhotos::orderBy('created_at', 'desc')->paginate(16);

        return response()->json($photos);
    }

    /**
     * List of ids from favorite photos
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function favoriteIds()
    {
        $userID = Auth::user()->id;
        $photoIds = FavoritePhotos::where('user_id', $userID)->get()->pluck('photo_id');

        return response()->json($photoIds);
    }

    /**
     * Favorite a particular photo
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function favoritePhoto(Request $request)
    {
        $userID = Auth::user()->id;

        $validatedData = $request->validate([
            'id' => 'required',
            'thumbnail' => 'required',
            'path' => 'required',
        ]);

        try {
            $ifExist = FavoritePhotos::where('photo_id', $request->id)->where('user_id', $userID)->first();
            
            if ($ifExist) {
                return response()->json(['message' => 'Failed! Alredy exist to Favorites'], 400);
            }

            Auth::user()->favorites()->create([
                'photo_id' => $request->id,
                'photo_url' => $request->thumbnail,
                'photo_url_thumbnail' => $request->path,
            ]);

            return response()->json(['message' => 'Success'], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['message' => 'Failed! ' . $th->getMessage()], 400);
        }

    }

    /**
     * Unfavorite a particular photo
     *
     * @param  Parameter Photo ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function unFavoritePhoto($photoID)
    {
        $userID = Auth::user()->id;
        try {
            FavoritePhotos::where('id', $photoID)->where('user_id', $userID)->delete();

            return response()->json(['message' => 'Success'], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['message' => 'Failed'], 400);
        }
    }

}
