<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoritePhotos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo_id', 'photo_url_thumbnail', 'photo_url', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
