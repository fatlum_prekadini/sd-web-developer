# StudyDrive Web Developer

## Features

- Laravel 7
- Vue + VueRouter + Vuex + axios + ESlint
- Pages with dynamic import and custom layouts
- Guests:
    - They can see first page which on weekdays shows only users ordered by the number of images marked as favourites and on weekends shows all favorite images marked by registred users.
    - Login, register and forgot password(reset password)
- Registred users:
    - After they login they can see all images from JSONPlaceholder and can mark favorites
    - Also they have my favorites page with the images they marked as favorites
    - Profile(settings)
    - Change password
- Authentication with JWT
- Bootstrap 4 + Font Awesome 5

## Installation

- `git clone https://fatlum_prekadini@bitbucket.org/fatlum_prekadini/sd-web-developer.git`
- Edit `.env` and set your database connection details
- `composer install`
- `php artisan key:generate` and `php artisan jwt:secret`
- `php artisan migrate --seed`
- `npm install`

## Usage

#### Login users
- login: `user@studydrive.com`, pass: `password`
- login: `user1@studydrive.com`, pass: `password`
- login: `user2@studydrive.com`, pass: `password`
- login: `user3@studydrive.com`, pass: `password`
- login: `user4@studydrive.com`, pass: `password`
- login: `user5@studydrive.com`, pass: `password`
- login: `user6@studydrive.com`, pass: `password`
- login: `user7@studydrive.com`, pass: `password`
- login: `user8@studydrive.com`, pass: `password`
- login: `user9@studydrive.com`, pass: `password`

#### Development

```bash
# build and watch
npm run watch
```

#### Production

```bash
npm run production
```

#### Frontend code style

```bash
# is checked through eslint
npm run lint
```

#### Test

```bash
php artisan test
```