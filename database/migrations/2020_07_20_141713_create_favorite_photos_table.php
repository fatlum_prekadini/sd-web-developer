<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavoritePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorite_photos', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->integer('photo_id');
            $table->string('photo_url_thumbnail');
            $table->string('photo_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorite_photos');
    }
}
