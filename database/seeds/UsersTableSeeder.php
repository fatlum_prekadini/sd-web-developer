<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => Str::random(10),
                'email' => 'user@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user1@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user2@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user3@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user4@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user5@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user6@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user7@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user8@studydrive.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => Str::random(10),
                'email' => 'user9@studydrive.com',
                'password' => Hash::make('password'),
            ],
        ]);
    }
}
